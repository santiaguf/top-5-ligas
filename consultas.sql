-- cantidad de equipos por patrocinador
SELECT COUNT(e.id_equipo),nombre_patrocinador FROM `patrocinadores` p 
JOIN equipos e ON p.id_patrocinador=e.id_patrocinador
GROUP BY p.nombre_patrocinador
ORDER BY COUNT(e.id_equipo) DESC


-- equipos nike
SELECT p.id_patrocinador,p.nombre_patrocinador,e.nombre_equipo,l.nombre_liga FROM `patrocinadores` p 
JOIN equipos e ON p.id_patrocinador=e.id_patrocinador
JOIN ligas l ON e.id_liga=l.id_liga
where p.id_patrocinador=2

-- equipos adidas
SELECT p.id_patrocinador,p.nombre_patrocinador,e.nombre_equipo,l.nombre_liga FROM `patrocinadores` p 
JOIN equipos e ON p.id_patrocinador=e.id_patrocinador
JOIN ligas l ON e.id_liga=l.id_liga
where p.id_patrocinador=4

-- equipos puma
SELECT p.id_patrocinador,p.nombre_patrocinador,e.nombre_equipo,l.nombre_liga FROM `patrocinadores` p 
JOIN equipos e ON p.id_patrocinador=e.id_patrocinador 
JOIN ligas l ON e.id_liga=l.id_liga 
where p.id_patrocinador=7

-- cantidad patrocinadores en ligue 1
select COUNT(DISTINCT nombre_patrocinador) from patrocinadores p 
JOIN equipos e ON p.id_patrocinador=e.id_patrocinador 
where e.id_liga=3

-- de dónde son los patrocinadores
SELECT DISTINCT(p.nombre_patrocinador),a.nombre_pais FROM `patrocinadores` p 
JOIN equipos e ON p.id_patrocinador=e.id_patrocinador
JOIN paises a ON p.pais_patrocinador=a.id_pais

-- a cuántos equipos patrocina cada país (suma de patrocinadores)
SELECT COUNT(e.id_equipo),a.nombre_pais FROM `patrocinadores` p 
JOIN equipos e ON p.id_patrocinador=e.id_patrocinador 
JOIN paises a ON p.pais_patrocinador=a.id_pais 
GROUP BY a.nombre_pais 
ORDER BY COUNT(e.id_equipo) DESC