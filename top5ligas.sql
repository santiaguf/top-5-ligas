--
-- Base de datos: `test`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `equipos`
--

CREATE TABLE `equipos` (
  `id_equipo` int(11) NOT NULL,
  `nombre_equipo` varchar(40) NOT NULL,
  `id_liga` int(2) NOT NULL,
  `id_patrocinador` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `equipos`
--

INSERT INTO `equipos` (`id_equipo`, `nombre_equipo`, `id_liga`, `id_patrocinador`) VALUES
(1, 'Athletic Club', 1, 6),
(2, 'Atlético de Madrid', 1, 2),
(3, 'C. D. Leganés', 1, 3),
(4, 'Deportivo Alavés', 1, 1),
(5, 'F. C. Barcelona', 1, 2),
(6, 'Getafe C. F.', 1, 3),
(7, 'Girona F. C.', 1, 8),
(8, 'Levante U.D.', 1, 5),
(9, 'Rayo Vallecano', 1, 1),
(10, 'R. C. Celta de Vigo', 1, 4),
(11, 'R. C. D. Español', 1, 1),
(12, 'Real Betis Balompié', 1, 9),
(13, 'Real Madrid C. F.', 1, 4),
(14, 'Real Sociedad', 1, 5),
(15, 'S. D. Eibar', 1, 7),
(16, 'S. D. Huesca', 1, 1),
(17, 'Sevilla F. C.', 1, 2),
(18, 'Valencia C. F.', 1, 4),
(19, 'Real Valladolid', 1, 10),
(20, 'Villarreal C. F.', 1, 3),
(21, 'Atalanta', 2, 3),
(22, 'Bologna', 2, 5),
(23, 'Cagliari', 2, 5),
(24, 'Chievo Verona', 2, 11),
(25, 'Empoli', 2, 3),
(26, 'Fiorentina', 2, 14),
(27, 'Genoa', 2, 12),
(28, 'Internazionale', 2, 2),
(29, 'Juventus', 2, 4),
(30, 'Lazio', 2, 5),
(31, 'Milan', 2, 7),
(32, 'Napoli', 2, 9),
(33, 'Parma', 2, 13),
(34, 'Roma', 2, 2),
(35, 'Sampdoria', 2, 3),
(36, 'Sassuolo', 2, 9),
(37, 'SPAL', 2, 5),
(38, 'Torino', 2, 9),
(39, 'Udinese', 2, 5),
(40, 'Frosinone', 2, 21),
(41, 'Amiens S. C.', 3, 7),
(42, 'Angers S. C. O.', 3, 9),
(43, 'Toulouse F. C.', 3, 3),
(44, 'S. M. Caen', 3, 8),
(45, 'Dijon F. C. O.', 3, 12),
(46, 'F. C. Girondins de Bordeaux', 3, 7),
(47, 'E. A. Guingamp', 3, 15),
(48, 'Lille O. S. C.', 3, 6),
(49, 'A. S. Mónaco', 3, 2),
(50, 'Montpellier Hérault S. C.', 3, 2),
(51, 'F. C. Nantes', 3, 6),
(52, 'Nîmes Olympique F. C.', 3, 7),
(53, 'O. G. C. Niza', 3, 5),
(54, 'Olympique de Lyon', 3, 4),
(55, 'Olympique de Marsella', 3, 7),
(56, 'París Saint-Germain', 3, 2),
(57, 'A. S. Saint-Étienne', 3, 14),
(58, 'R. C. Estrasburgo', 3, 4),
(59, 'Stade Rennais F. C.', 3, 7),
(60, 'Stade de Reims', 3, 16),
(61, 'Arsenal', 4, 7),
(62, 'Bournemouth', 4, 8),
(63, 'Brighton & Hove Albion', 4, 2),
(64, 'Burnley', 4, 7),
(65, 'Cardiff City', 4, 4),
(66, 'Chelsea', 4, 2),
(67, 'Crystal Palace', 4, 7),
(68, 'Everton', 4, 8),
(69, 'Fulham', 4, 4),
(70, 'Huddersfield Town', 4, 8),
(71, 'Leicester City', 4, 4),
(72, 'Liverpool', 4, 6),
(73, 'Manchester City', 4, 2),
(74, 'Manchester United', 4, 4),
(75, 'Newcastle United', 4, 7),
(76, 'Southampton', 4, 18),
(77, 'Tottenham Hotspur', 4, 2),
(78, 'Watford', 4, 4),
(79, 'West Ham United', 4, 8),
(80, 'Wolverhampton Wanderers', 4, 4),
(81, 'Augsburgo', 5, 2),
(82, 'Bayer Leverkusen', 5, 19),
(83, 'Bayern de Múnich', 5, 4),
(84, 'Borussia Dortmund', 5, 7),
(85, 'Borussia Mönchengladbach', 5, 7),
(86, 'Eintracht Fráncfort', 5, 2),
(87, 'Fortuna Düsseldorf', 5, 20),
(88, 'Friburgo', 5, 10),
(89, 'Hannover 96', 5, 19),
(90, 'Hertha Berlín', 5, 2),
(91, 'Hoffenheim', 5, 12),
(92, 'Maguncia 05', 5, 12),
(93, 'Núremberg?', 5, 8),
(94, 'RB Leipzig', 5, 2),
(95, 'Schalke 04', 5, 8),
(96, 'Stuttgart', 5, 7),
(97, 'Werder Bremen', 5, 8),
(98, 'Wolfsburgo', 5, 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ligas`
--

CREATE TABLE `ligas` (
  `id_liga` int(2) NOT NULL,
  `nombre_liga` varchar(20) NOT NULL,
  `pais_liga` int(2) NOT NULL,
  `cantidad_equipos` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Volcado de datos para la tabla `ligas`
--

INSERT INTO `ligas` (`id_liga`, `nombre_liga`, `pais_liga`, `cantidad_equipos`) VALUES
(1, 'Liga Santander', 1, 20),
(2, 'Serie A', 2, 20),
(3, 'Ligue 1', 3, 20),
(4, 'Premier league', 4, 20),
(5, 'Bundesliga', 5, 18);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `paises`
--

CREATE TABLE `paises` (
  `id_pais` int(2) NOT NULL,
  `nombre_pais` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `paises`
--

INSERT INTO `paises` (`id_pais`, `nombre_pais`) VALUES
(1, 'España'),
(2, 'Italia'),
(3, 'Francia'),
(4, 'Inglaterra'),
(5, 'Alemania'),
(6, 'USA'),
(7, 'Dinamarca'),
(8, 'Bélgica');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `patrocinadores`
--

CREATE TABLE `patrocinadores` (
  `id_patrocinador` int(2) NOT NULL,
  `nombre_patrocinador` varchar(30) NOT NULL,
  `pais_patrocinador` int(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `patrocinadores`
--

INSERT INTO `patrocinadores` (`id_patrocinador`, `nombre_patrocinador`, `pais_patrocinador`) VALUES
(1, 'Kelme', 1),
(2, 'Nike', 6),
(3, 'Joma', 1),
(4, 'Adidas', 5),
(5, 'Macron', 2),
(6, 'New Balance', 6),
(7, 'Puma', 5),
(8, 'Umbro', 4),
(9, 'Kappa', 2),
(10, 'Hummel', 7),
(11, 'Givova', 2),
(12, 'Lotto', 2),
(13, 'Erreá', 2),
(14, 'Le Coq Sportif', 3),
(15, 'Patrick', 8),
(16, 'Hungaria Sport', 3),
(18, 'Under Armour', 6),
(19, 'Jako', 5),
(20, 'Uhlsport', 5),
(21, 'Legea', 2);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `equipos`
--
ALTER TABLE `equipos`
  ADD PRIMARY KEY (`id_equipo`),
  ADD KEY `id_pais` (`id_liga`),
  ADD KEY `id_patrocinador` (`id_patrocinador`);

--
-- Indices de la tabla `ligas`
--
ALTER TABLE `ligas`
  ADD PRIMARY KEY (`id_liga`),
  ADD KEY `pais_liga` (`pais_liga`);

--
-- Indices de la tabla `paises`
--
ALTER TABLE `paises`
  ADD PRIMARY KEY (`id_pais`);

--
-- Indices de la tabla `patrocinadores`
--
ALTER TABLE `patrocinadores`
  ADD PRIMARY KEY (`id_patrocinador`),
  ADD KEY `pais_patrocinador` (`pais_patrocinador`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `equipos`
--
ALTER TABLE `equipos`
  MODIFY `id_equipo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=100;

--
-- AUTO_INCREMENT de la tabla `ligas`
--
ALTER TABLE `ligas`
  MODIFY `id_liga` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT de la tabla `paises`
--
ALTER TABLE `paises`
  MODIFY `id_pais` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `patrocinadores`
--
ALTER TABLE `patrocinadores`
  MODIFY `id_patrocinador` int(2) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `equipos`
--
ALTER TABLE `equipos`
  ADD CONSTRAINT `equipos_ibfk_1` FOREIGN KEY (`id_liga`) REFERENCES `ligas` (`id_liga`),
  ADD CONSTRAINT `equipos_ibfk_2` FOREIGN KEY (`id_patrocinador`) REFERENCES `patrocinadores` (`id_patrocinador`);

--
-- Filtros para la tabla `ligas`
--
ALTER TABLE `ligas`
  ADD CONSTRAINT `ligas_ibfk_1` FOREIGN KEY (`pais_liga`) REFERENCES `paises` (`id_pais`);

--
-- Filtros para la tabla `patrocinadores`
--
ALTER TABLE `patrocinadores`
  ADD CONSTRAINT `patrocinadores_ibfk_1` FOREIGN KEY (`pais_patrocinador`) REFERENCES `paises` (`id_pais`);
COMMIT;